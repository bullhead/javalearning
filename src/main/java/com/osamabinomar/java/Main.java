package com.osamabinomar.java;

import io.reactivex.Flowable;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Flowable<Integer> integerFlowable = Flowable.range(1, 100)
                .map(integer -> integer * new Random().nextInt(100));
        integerFlowable.subscribe(System.out::println);
    }
}
