package com.osamabinomar.java.se.chap4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

interface I {
    void mI();
}

interface J {
    void mJ();
}

public class Generics {
    public static void main(String... args) {
        //4.4.1 Members of type variable
        Test test = new Test();
        test.test(new CTJ());
        test.test2(new CT());
        C c = new C();
        I i = () -> System.out.println("Hello I am joking");
        //END 4.4.1

        //4.8 Raw types
        Outer<Integer>.AnotherInner<Double> x = null;
        Cell cell = new Cell<String>("abc");
        cell.set("Hello");

//        RawMemebers rw=null;
//        Collection<Number> cn=rw.myNumbers();
//        Iterator<String> is=rw.iterator();
//        Collection<NonGeneric> cng=rw.cng;

        m(new ArrayList<>());

    }

    //heap pollution
    static void m(List<String>... stringLists) {
        Object[] array = stringLists;
        List<Integer> tmpList = Arrays.asList(42);
        array[0] = tmpList;                // (1)
        String s = stringLists[0].get(0);  // (2)
    }
}

/**
 * 4.4.1
 */
class C {
    public void mCPublic() {
        System.out.println("I am public in " + C.class.getCanonicalName());
    }

    protected void mCProtected() {
        System.out.println("I am protected in " + C.class.getCanonicalName());
    }

    void mCPackage() {
        System.out.println("I am accessible within same package in " + C.class.getCanonicalName());
    }

    private void mCPrivate() {
    }

}

class Test {
    <T extends C & I & J> void test(T t) {
        t.mCPublic();
        t.mCProtected();
        t.mCPackage();
        t.mI();
        t.mJ();
    }

    <T extends I> void test2(T t) {
        t.mI();
    }
}

class CT extends C implements I {

    @Override
    public void mI() {
        System.out.println("I was joking");
    }
}

class CTJ extends CT implements J {
    @Override
    public void mI() {

    }

    @Override
    public void mJ() {

    }
}
/*
 * END 4.4.1
 */

/**
 * Section 4.8 Raw types
 */
class Outer<T> {
    T t;

    class Inner {
        T setOuterT(T t1) {
            t = t1;
            return t;
        }
    }

    class AnotherInner<S> {
        S s;
    }
}

class Cell<E> {
    E value;

    Cell(E value) {
        this.value = value;
    }

    E get() {
        return value;
    }

    void set(E v) {
        this.value = v;
    }
}

class NonGeneric {
    Collection<Number> myNumbers() {
        return null;
    }
}

abstract class RawMemebers<T> extends NonGeneric implements Collection<String> {
    static Collection<NonGeneric> cng = new ArrayList<>();

}

//END 4.8


