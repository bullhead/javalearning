package com.osamabinomar.java.se.chap5;

public class Boxing {
    public static void main(String... args) {
        double p = Math.sqrt(-1); //should be NaN
        System.out.println(Double.valueOf(p) == p);
        Double b = Math.sqrt(-1);
        System.out.println(Math.sqrt(-1) == b);
        //String conversion
        Integer number = null;
        String text = "Hello => " + number;
        System.out.println(text);
        //Forbidden conversion
        //boolean bol=true;
        //double dol= (double) Double.valueOf(String.valueOf(bol));

    }
}
